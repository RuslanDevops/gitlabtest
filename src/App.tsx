import {ChangeEvent, FormEvent, useEffect, useState} from 'react';
import axios, {AxiosResponse} from 'axios';
import './App.css';

type TUserPre = {
    name: string,
    email: string
}
const App = () => {
  const baseUrl = import.meta.env.VITE_BASE_URL;
  const [userData, setUserData] = useState<TUserPre>({
      name: "",
      email: ""
      }
  )
  const [users, setUsers] = useState<IUser[]>([]);

  const changeUserDataHandler = (e:  ChangeEvent<HTMLInputElement>) => {
      setUserData(prevState => ({
          ...prevState, [e.target.name]: e.target.value
      }));
  }

  interface IUser {
      id: string,
      name: string,
      email: string
  }
  const fechData = async () => {
      try {
          const response:AxiosResponse<any> = await axios.get(baseUrl);
          response.status < 400 && setUsers(response.data);
      } catch (err: unknown) {
          const error: Error = err as Error;
          console.log(error.message);
      }
  }

    const addUserHandler = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            if (userData.name.trim().length && userData.email.trim().length) {
                const response:AxiosResponse<any> = await axios.post(baseUrl, userData);
                const data: IUser = response.data;
                data && setUsers(prevState => ([
                    ...prevState, data
                ]));
            } else {
                alert("Заполните все поля")
            }
        } catch (err: unknown) {
            const error:Error = err as Error;
            console.log(error.message);
            console.log("Catch Error", baseUrl);
        }
    }

    useEffect(() => {
        fechData();
    }, []);

  return (
    <>
      <h1>Подготовил проект на сервер CI-CD</h1>
      <div className="card">
          <form onSubmit={addUserHandler} className={"mainForm"}>
              <label htmlFor={"name"}>Name</label>
              <input
                  id="name"
                  className={"mainInput"}
                  type={"text"} name={"name"}
                  onChange={changeUserDataHandler}
                  value={userData.name}/>
              <label htmlFor={"email"}>Email</label>
              <input
                  id="email"
                  className={"mainInput"}
                  type={"text"} name={"email"}
                  onChange={changeUserDataHandler}
                  value={userData.email}/>
              <button type={"submit"}>Send</button>
          </form>
          {users && users.map(user => {
              return <p key={user.id}>{user.name}</p>
          })}
      </div>
    </>
  )
}

export default App;